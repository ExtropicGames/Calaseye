# Find libtcod
# Find the libtcod includes and library
#
#  LIBTCOD_INCLUDE_DIRS - where to find libtcod.h, etc.
#  LIBTCOD_LIBRARIES    - List of libraries when using libtcod.
#  LIBTCOD_FOUND        - True if libtcod found.

IF (LIBTCOD_INCLUDE_DIR)
  # Already in cache, be silent
  SET(LIBTCOD_FIND_QUIETLY TRUE)
ENDIF (LIBTCOD_INCLUDE_DIR)

FIND_PATH(LIBTCOD_INCLUDE_DIR libtcod.hpp PATH_SUFFIXES include)

SET(LIBTCOD_NAMES TCOD tcod TCOD Tcod tcod)
FIND_LIBRARY(LIBTCOD_LIBRARY NAMES ${LIBTCOD_NAMES} )
MARK_AS_ADVANCED(LIBTCOD_LIBRARY LIBTCOD_INCLUDE_DIR)

# Per-recommendation
SET(LIBTCOD_INCLUDE_DIRS "${LIBTCOD_INCLUDE_DIR}")
SET(LIBTCOD_LIBRARIES    "${LIBTCOD_LIBRARY}")

# handle the QUIETLY and REQUIRED arguments and set SOIL_FOUND to TRUE if
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(LIBTCOD DEFAULT_MSG LIBTCOD_LIBRARIES LIBTCOD_INCLUDE_DIRS)
