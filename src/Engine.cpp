#include <libtcod.hpp>
#include "Entity/Entity.hpp"
#include "Entity/PlayerEntity.hpp"
#include "Entity/AI/PlayerAI.hpp"
#include "Map.hpp"
#include "Gui.hpp"
#include "Engine.hpp"

Engine::Engine(int screenWidth, int screenHeight) : gameStatus(STARTUP), fovRadius(10), screenWidth(screenWidth), screenHeight(screenHeight) {
  TCODConsole::setCustomFont("terminal.png", TCOD_FONT_LAYOUT_ASCII_INROW);
  TCODConsole::initRoot(screenWidth, screenHeight, "Calaseye", false);

  player = new PlayerEntity(40, 25, '@', "player", TCODColor::white);

  player->destructible = new PlayerDestructible(30, 2, "your cadaver");
  player->attacker = new Attacker(5);
  player->ai = new PlayerAi();
  player->container = new Container(26);
  entities.push(player);
  map = new Map(80,43);
  gui = new Gui();
  gui->message(TCODColor::red, "Welcome to the world of Calaseye.");
}

Engine::~Engine() {
  entities.clearAndDelete();
  delete map;
  delete gui;
}

void Engine::update() {
  if (gameStatus == STARTUP) {
    map->computeFov();
  }
  gameStatus = IDLE;
  TCODSystem::checkForEvent(TCOD_EVENT_KEY_PRESS|TCOD_EVENT_MOUSE, &lastKey, &mouse);
  if (lastKey.vk == TCODK_ENTER && lastKey.lalt) {
    TCODConsole::setFullscreen(!TCODConsole::isFullscreen());
  }
  entity_update(player);
  if (gameStatus == NEW_TURN) {
    for (Entity* entity : entities) {
      if (entity != player) {
        entity_update(entity);
      }
    }
  }
}

void Engine::render() {
  TCODConsole::root->clear();
  // draw the map
  map->render();
  // draw the actors
  for (Entity* entity : entities) {
    if (entity != player && map->isInFov(entity->x, entity->y)) {
      entity->render();
    }
  }
  player->render();
  // show the player's stats
  gui->render();
}

void Engine::sendToBack(Entity* entity) {
  entities.remove(entity);
  entities.insertBefore(entity, 0);
}

Entity* Engine::getEntity(int x, int y) const {
  for (Entity* entity : entities) {
    if (entity->x == x && entity->y == y && is_alive(entity)) {
      return entity;
    }
  }
  return nullptr;
}

Entity * Engine::getClosestMonster(int x, int y, float range) const {
  Entity *closest = nullptr;
  float bestDistance = 1E6f;
  for (Entity *entity : entities) {
    if (entity != player && is_alive(entity)) {
      float distance = get_distance(entity, x, y);
      if (distance < bestDistance && (distance <= range || range == 0.0f)) {
        bestDistance = distance;
        closest = entity;
      }
    }
  }
  return closest;
}

/**
 * @return True if a tile was selected, false if cancelled
 */
bool Engine::pickATile(int *x, int *y, float maxRange) {
  while ( !TCODConsole::isWindowClosed() ) {
    render();
    // highlight the possible range
    for (int cx=0; cx < map->width; cx++) {
      for (int cy=0; cy < map->height; cy++) {
        if (map->isInFov(cx,cy) && ( maxRange == 0 || get_distance(player, cx, cy) <= maxRange) ) {
          TCODColor col=TCODConsole::root->getCharBackground(cx,cy);
          col = col * 1.2f;
          TCODConsole::root->setCharBackground(cx,cy,col);
        }
      }
    }

    // TODO: add support for keyboard cursor here too
    TCODSystem::checkForEvent(TCOD_EVENT_KEY_PRESS|TCOD_EVENT_MOUSE,&lastKey,&mouse);
    if (map->isInFov(mouse.cx,mouse.cy) && ( maxRange == 0 || get_distance(player, mouse.cx, mouse.cy) <= maxRange )) {
      TCODConsole::root->setCharBackground(mouse.cx, mouse.cy, TCODColor::white);
      if (mouse.lbutton_pressed) {
        *x = mouse.cx;
        *y = mouse.cy;
        return true;
      }
    }
    if (mouse.rbutton_pressed || lastKey.vk != TCODK_NONE) {
      return false;
    }
    TCODConsole::flush();
  }
  return false;
}

//! @param x
//! @param y - Starting location of the cursor
//! @param maxRange <= 0 means infinite range
Entity* Engine::targetPerson(float maxRange) {

  // Run a game loop until the method is done
  while (!TCODConsole::isWindowClosed()) {
    render();

    // highlight the possible range
    for (int cx = 0; cx < this->map->width; cx++) {
      for (int cy = 0; cy < this->map->height; cy++) {
        if (this->map->isInFov(cx,cy) && (maxRange <= 0 || get_distance(this->player, cx, cy) <= maxRange) ) {
          TCODColor col = TCODConsole::root->getCharBackground(cx,cy) * 1.2f;
          TCODConsole::root->setCharBackground(cx,cy,col);
        }
      }
    }

    // TODO: add support for keyboard cursor here too
    TCODSystem::checkForEvent(TCOD_EVENT_KEY_PRESS|TCOD_EVENT_MOUSE,&lastKey,&mouse);
    if (map->isInFov(mouse.cx,mouse.cy) && ( maxRange == 0 || get_distance(player, mouse.cx, mouse.cy) <= maxRange )) {
      TCODConsole::root->setCharBackground(mouse.cx, mouse.cy, TCODColor::white);
      if (mouse.lbutton_pressed) {
        TCODList<Entity*> potentialEntities;
        for (Entity* entity : engine.entities) {
          if (entity->x == mouse.cx && entity->y == mouse.cy && is_alive(entity)) {
            potentialEntities.push(entity);
          }
        }
        if (potentialEntities.size() == 1) {
          return potentialEntities.pop();
        }
        if (potentialEntities.size() > 1) {
          int index = this->gui->listSelector(potentialEntities);
          if (index < 0) {
            return nullptr;
          }
          return potentialEntities.get(index);
        }
        return nullptr;
      }
    }
    if (mouse.rbutton_pressed || lastKey.vk != TCODK_NONE) {
      return nullptr;
    }
    TCODConsole::flush();
  }
  return nullptr;
}
