
#include "Entity/Entity.hpp"
#include "Entity/Components/Destructible.hpp"

Entity* upStairs(int x, int y) {
  Entity* e = new Entity(x, y, '<', "up stairs", TCODColor::grey);
  e->destructible = new Indestructible();
  return e;
}