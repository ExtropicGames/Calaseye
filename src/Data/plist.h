#pragma once

#include <stdint.h>

typedef struct plist_item {
  void* item;
  uint64_t rarity;
} plist_item;

typedef struct plist {
  plist_item* items;
  uint32_t size;
} plist;

plist_item* pick_item(plist* list);
