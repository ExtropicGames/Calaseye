
#include "plist.h"

#include <assert.h>
#include <stdlib.h>

plist_item* pick_item(plist* list) {
  uint32_t i;
  uint64_t p;
  while(1) {
    // TODO: replace rand with something better
    i = rand() % list->size;
    p = rand() % RAND_MAX; // TODO
    assert(list->items[i].rarity < RAND_MAX); // Make sure that no item is too rare to exist
    if (p < RAND_MAX - list->items[i].rarity) {
      return &list->items[i];
    }
  }
}
