
#include "Engine.hpp"
#include "Entity/Entity.hpp"
#include "Entity/AI/MonsterAI.hpp"
#include "Entity/AI/ConfusedMonsterAI.hpp"

typedef struct {
  const char*      name;
  const char       icon;
  const TCODColor* color;
  const char*      corpseName;
  const int        listLevel;
  const int        rarity;
  const int        hp;
  const int        defense;
  const int        attack;
} monster;

static const monster monsters[] = {
  {
    .name       = "farmer",
    .icon       = 'f',
    .color      = &TCODColor::brass,
    .corpseName = "dead farmer",
    .listLevel  = 1,
    .rarity     = 20,
    .hp         = 10,
    .defense    = 0,
    .attack     = 3,
  },
  {
    .name       = "blaze",
    .icon       = 'B',
    .color      = &TCODColor::lightRed,
    .corpseName = "blaze ember",
    .listLevel  = 1,
    .rarity     = 80,
    .hp         = 16,
    .defense    = 1,
    .attack     = 4,
  }
};

Entity* makeMonster(int x, int y) {
  TCODRandom *rng = TCODRandom::getInstance();
  const monster* m;

  // TODO: calculate lists based on dungeon level and use rarity to pick from list
  if (rng->getInt(0,100) < 80) {
    m = &monsters[0]; // farmer
  } else {
    m = &monsters[1]; // blaze
  }

  Entity* e = new Entity(x, y, m->icon, m->name, *m->color);
  e->destructible = new MonsterDestructible(m->hp, m->defense, m->corpseName);
  e->attacker = new Attacker(m->attack);
  e->ai = new MonsterAi();
  return e;
}

Entity* makeItem(int x, int y) {
  TCODRandom *rng = TCODRandom::getInstance();

  char* name;
  char  icon;
  TCODColor color;
  bool blocks;
  TargetSelector* targetSelector;
  Effect* effect;

  int dice = rng->getInt(0,100);
  if ( dice < 70 ) {
    name           = "health potion";
    icon           = '!';
    color          = TCODColor::violet;
    blocks         = false;
    targetSelector = nullptr;
    effect         = new HealthEffect(4, nullptr);
  } else if ( dice < 70+10 ) {
    name           = "scroll of lightning bolt";
    icon           = '#';
    color          = TCODColor::lightYellow;
    blocks         = false;
    targetSelector = new TargetSelector(TargetSelector::CLOSEST_MONSTER, 5);
    effect         = new HealthEffect(-20, "A lighting bolt strikes the %s with a loud thunder!\n" "The damage is %g hit points.");
  } else if ( dice < 70+10+10 ) {
    name           = "scroll of fireball";
    icon           = '#';
    color          = TCODColor::lightYellow;
    blocks         = false;
    targetSelector = new TargetSelector(TargetSelector::SELECTED_RANGE, 3);
    effect         = new HealthEffect(-12, "The %s gets burned for %g hit points.");
  } else {
    name           = "scroll of confusion";
    icon           = '#';
    color          = TCODColor::lightYellow;
    blocks         = false;
    targetSelector = new TargetSelector(TargetSelector::SELECTED_MONSTER, 5);
    effect         = new AiChangeEffect(new ConfusedMonsterAi(10), "The eyes of the %s look vacant,\nas he starts to stumble around!");
  }

  Entity* e = new Entity(x, y, icon, name, color);
  e->blocks = blocks;
  e->pickable = new Pickable(targetSelector, effect);
  return e;
}