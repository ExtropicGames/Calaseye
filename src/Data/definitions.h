
struct entity;
struct brain;

// powers

enum selector_type {
  CLOSEST_ENTITY,
  TARGET_ENTITY,
  OWNER_RADIUS,
  TARGET_RADIUS,
};

struct selector {
  selector_type type;
  uint32_t range; // Also radius
};

//typedef effect_action void ()();

struct power_effect {
  //effect_action type;
};

struct power {
  selector selector;
  power_effect effect;
};

// brains



union brain_state {
  struct {
    uint64_t turns_remaining; // How long until the temporary state wears off
    brain* old_brain; // Which brain to switch back to once this wears off
  }; // temporary AI
};

//typedef brain_action void ()(world_state*, brain_state*);

struct brain {
  //brain_action act;
  brain_state state;
};

// entities

struct soul {
  int64_t hearth;
  int64_t eternity;
};

struct inventory {
  entity* items;
  uint32_t size;
};

struct entity {
  struct { // properties common to all entities
    int64_t x;
    int64_t y;
    char icon; // ascii/unicode icon
    TCODColor color;
    bool blocking; // does this block other entities from occupying the same space?

    // could become optional properties
    int64_t damage; // how much damage the entity has accrued
    power* powers;
    uint32_t power_count;
  };

  // optional properties
  soul* soul;
  inventory* inventory;
  brain* brain;
};
