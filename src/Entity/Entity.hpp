#pragma once

#include <libtcod.hpp>

#include "AI/AI.hpp"
#include "Components/Attacker.hpp"
#include "Components/Destructible.hpp"
#include "Components/Pickable.hpp"
#include "Components/Container.hpp"

#include "Data/definitions.h"

//! An entity represents anything that can appear on the map, whether they are items, people, or other objects.
class Entity {
public:
  int x, y; // position on map
  int ch; // ascii code
  TCODColor col; // color
  bool blocks; // can we walk on this entity?
  Attacker* attacker; // something that deals damages
  Destructible* destructible; // something that can be damaged
  Ai* ai; // something self-updating
  Pickable* pickable; // something that can be picked and used
  Container* container; // something that can contain entity
  soul* soul; // set of attributes that affect entity's stats

  Entity(int x, int y, int ch, const char *name, const TCODColor &col);
  ~Entity();
  void render() const;

  virtual const char* name() {
    return _name;
    //return strcat("the", name);
  }

  // Most entities don't have a proper name.
  virtual const char* properName() {
    return name();
  }

protected:
  const char* _name; // the entity's name
};

void entity_update(Entity* e);
bool is_alive(const Entity* e);
float get_distance(const Entity* e, int x, int y);
