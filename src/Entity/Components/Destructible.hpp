#pragma once

class Destructible {
public :
	float maxHp; // maximum health points
	float hp; // current health points
	float defense; // hit points deflected
  const char *corpseName; // the actor's name once dead/destroyed

  Destructible(float maxHp, float defense, const char *corpseName);
  inline bool isDead() { return hp <= 0; }

  virtual float calculateDamage(Entity* owner, float damage);
  float applyDamage(Entity *owner, float damage);
  float heal(float amount);
  virtual void die(Entity *owner);
};

class Indestructible : public Destructible {
public:
  Indestructible();
  float calculateDamage(Entity* owner, float damage) {
    return 0;
  }
};

class MonsterDestructible : public Destructible {
public:
	MonsterDestructible(float maxHp, float defense, const char *corpseName);
	void die(Entity *owner);
};

class PlayerDestructible : public Destructible {
public:
	PlayerDestructible(float maxHp, float defense, const char *corpseName);
	void die(Entity *owner);
};
