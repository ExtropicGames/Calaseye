#pragma once

class Entity;

class Attacker {
public :
	float power; // hit points given

	Attacker(float power);
	void attack(Entity *owner, Entity *target);
};
