#pragma once

class Container {
public :
	int size; // maximum number of entities. 0 = unlimited
	TCODList<Entity*> inventory;

	Container(int size);
	~Container();
	bool add(Entity* entity);
	void remove(Entity* entity);
};
