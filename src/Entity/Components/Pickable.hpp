#pragma once

#include "Entity/AI/TemporaryAI.hpp"

class TargetSelector {
public :
	enum SelectorType {
		CLOSEST_MONSTER,
		SELECTED_MONSTER,
		WEARER_RANGE,
		SELECTED_RANGE
	};
	TargetSelector(SelectorType type, float range);
	void selectTargets(Entity *wearer, TCODList<Entity *> & list);
protected :
	SelectorType type;
	float range;
};

class Effect {
public :
	virtual bool applyTo(Entity *actor) = 0;
};

class HealthEffect : public Effect {
public :
	float amount;
	const char *message;

	HealthEffect(float amount, const char *message);
	bool applyTo(Entity *actor);
};

class AiChangeEffect : public Effect {
public :
	TemporaryAi *newAi;
	const char *message;

	AiChangeEffect(TemporaryAi *newAi, const char *message);
	bool applyTo(Entity *actor);
};

class Pickable {
public :
	Pickable(TargetSelector *selector, Effect *effect);
	virtual ~Pickable();
	bool pick(Entity *owner, Entity *wearer);
	void drop(Entity *owner, Entity *wearer);
	bool use(Entity *owner, Entity *wearer);
protected :
	TargetSelector *selector;
	Effect *effect;
};