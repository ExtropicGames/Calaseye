#include <libtcod.hpp>
#include "../Entity.hpp"

Container::Container(int size) : size(size) {
}

Container::~Container() {
  inventory.clearAndDelete();
}

bool Container::add(Entity* entity) {
  if ( size > 0 && inventory.size() >= size ) {
    // inventory full
    return false;
  }
  inventory.push(entity);
  return true;
}

void Container::remove(Entity* entity) {
  inventory.remove(entity);
}
