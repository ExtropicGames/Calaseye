#include <libtcod.hpp>
#include "../Entity.hpp"
#include "../../Gui.hpp"
#include "../../Engine.hpp"

TargetSelector::TargetSelector(SelectorType type, float range)
	: type(type), range(range) {
}

void TargetSelector::selectTargets(Entity *wearer, TCODList<Entity *> & list) {
	switch (type) {
		case CLOSEST_MONSTER: {
			Entity *closestMonster=engine.getClosestMonster(wearer->x,wearer->y,range);
			if (closestMonster) {
				list.push(closestMonster);
			}
		}
		break;
		case SELECTED_MONSTER: {
			int x, y;
			engine.gui->message(TCODColor::cyan, "Left-click to select an enemy,\nor right-click to cancel.");
			if (engine.pickATile(&x,&y,range)) {
        Entity* actor = engine.getEntity(x,y);
        if (actor) {
					list.push(actor);
				}
			}
		}
		break;
		case WEARER_RANGE: {
			for (Entity* e : engine.entities) {
        if (is_alive(e) && get_distance(e, wearer->x, wearer->y) <= range) {
          list.push(e);
        }
      }
		}
		break;
    case SELECTED_RANGE: {
			// TODO: extract this out to separate reusable function in Map.cpp
			int x, y;
			engine.gui->message(TCODColor::cyan, "Left-click to select a tile,\nor right-click to cancel.");
			if (engine.pickATile(&x, &y)) {
				for (Entity* entity : engine.entities) {
					if (is_alive(entity) && get_distance(entity, x, y) <= range) {
						list.push(entity);
					}
				}
			}
		}
		break;
	}
	if (list.isEmpty()) {
		engine.gui->message(TCODColor::lightGrey, "No enemy is close enough");
	}
}

HealthEffect::HealthEffect(float amount, const char *message)
	: amount(amount), message(message) {
}

bool HealthEffect::applyTo(Entity *actor) {
	if (!actor->destructible) return false;
	if (amount > 0) {
		float pointsHealed=actor->destructible->heal(amount);
		if (pointsHealed > 0) {
			if (message) {
				engine.gui->message(TCODColor::lightGrey, message, actor->name(), pointsHealed);
			}
			return true;
		}
	} else {
		if (message && -amount-actor->destructible->defense > 0) {
			engine.gui->message(TCODColor::lightGrey, message, actor->name(), -amount-actor->destructible->defense);
		}
		float damage = actor->destructible->calculateDamage(actor, -amount);
		actor->destructible->applyDamage(actor, -amount);
		if (damage > 0) {
			return true;
		}
	}
	return false;
}

AiChangeEffect::AiChangeEffect(TemporaryAi *newAi, const char *message)
	: newAi(newAi), message(message) {
}

bool AiChangeEffect::applyTo(Entity *actor) {
	newAi->applyTo(actor);
	if (message) {
		engine.gui->message(TCODColor::lightGrey, message, actor->name());
	}
	return true;
}

Pickable::Pickable(TargetSelector *selector, Effect *effect) :
	selector(selector), effect(effect) {
}

Pickable::~Pickable() {
	if (selector) { delete selector; }
	if (effect) { delete effect; }
}

bool Pickable::pick(Entity *owner, Entity *wearer) {
	if (wearer->container && wearer->container->add(owner)) {
		engine.entities.remove(owner);
		return true;
	}
	return false;
}

void Pickable::drop(Entity *owner, Entity *wearer) {
	if (wearer->container) {
		wearer->container->remove(owner);
		engine.entities.push(owner);
		owner->x = wearer->x;
		owner->y = wearer->y;
		engine.gui->message(TCODColor::lightGrey,"%s drops a %s.", wearer->name(), owner->name());
	}
}

bool Pickable::use(Entity *owner, Entity *wearer) {
	TCODList<Entity *> list;
	if (selector) {
		selector->selectTargets(wearer, list);
	} else {
		list.push(wearer);
	}
	bool succeed=false;
  for (Entity* e : list) {
    if (effect->applyTo(e)) {
      succeed=true;
    }
  }
	if ( succeed ) {
		if ( wearer->container ) {
			wearer->container->remove(owner);
			delete owner;
		}
	}
	return succeed;
}
