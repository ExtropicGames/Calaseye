#include <libtcod.hpp>
#include "../Entity.hpp"
#include "../../Gui.hpp"
#include "../../Engine.hpp"

Attacker::Attacker(float power) : power(power) {
}

void Attacker::attack(Entity *owner, Entity *target) {
  if (!is_alive(target)) {
    engine.gui->message(TCODColor::lightGrey, "%s attacks %s in vain.", owner->name(), target->name());
    return;
  }

  float damage = target->destructible->calculateDamage(target, power);

  if (damage > 0) {
    TCODColor col = owner == engine.player ? TCODColor::red : TCODColor::lightGrey;
    engine.gui->message(col, "%s attacks %s for %g hit points.", owner->name(), target->name(), damage);
  } else {
    engine.gui->message(TCODColor::lightGrey, "%s attacks %s but it has no effect!", owner->name(), target->name());
  }

  target->destructible->applyDamage(target, power);
}
