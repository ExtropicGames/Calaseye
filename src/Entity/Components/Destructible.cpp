#include <libtcod.hpp>
#include "../Entity.hpp"
#include "../../Gui.hpp"
#include "../../Engine.hpp"

Destructible::Destructible(float maxHp, float defense, const char *corpseName) : maxHp(maxHp),hp(maxHp),defense(defense),corpseName(corpseName) {
}

float Destructible::calculateDamage(Entity* owner, float damage) {
  damage -= defense;

  if (damage < 0) {
    damage = 0;
  }

  return damage;
}

float Destructible::applyDamage(Entity *owner, float damage) {
  damage = calculateDamage(owner, damage);

  hp -= damage;
  if ( hp <= 0 ) {
    die(owner);
  }

  return damage;
}

float Destructible::heal(float amount) {
  hp += amount;
  if ( hp > maxHp ) {
    amount -= hp-maxHp;
    hp=maxHp;
  }
  return amount;
}

void Destructible::die(Entity *owner) {
  // transform the actor into a corpse!
  owner->ch = '%';
  owner->col = TCODColor::darkRed;
  //owner->name = corpseName;
  owner->blocks = false;
  // make sure corpses are drawn before living actors
  engine.sendToBack(owner);
}

Indestructible::Indestructible() : Destructible(1, 1, "") {}

MonsterDestructible::MonsterDestructible(float maxHp, float defense, const char *corpseName) : Destructible(maxHp,defense,corpseName) {
}

void MonsterDestructible::die(Entity *owner) {
  // transform it into a nasty corpse! it doesn't block, can't be
  // attacked and doesn't move
  engine.gui->message(TCODColor::lightGrey, "%s is dead", owner->name());
  Destructible::die(owner);
}

PlayerDestructible::PlayerDestructible(float maxHp, float defense, const char *corpseName) : Destructible(maxHp,defense,corpseName) {
}

void PlayerDestructible::die(Entity *owner) {
  engine.gui->message(TCODColor::red, "You died!");
  Destructible::die(owner);
  engine.gameStatus=Engine::DEFEAT;
}
