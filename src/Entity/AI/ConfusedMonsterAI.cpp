#include <libtcod.hpp>

#include "Engine.hpp"
#include "Entity/Entity.hpp"
#include "Map.hpp"
#include "Entity/AI/TemporaryAI.hpp"
#include "Entity/AI/ConfusedMonsterAI.hpp"

ConfusedMonsterAi::ConfusedMonsterAi(int nbTurns) : TemporaryAi(nbTurns) {
}

void ConfusedMonsterAi::update(Entity* owner) {
  TCODRandom *rng = TCODRandom::getInstance();
  int dx = rng->getInt(-1, 1);
  int dy = rng->getInt(-1, 1);
  if (dx != 0 || dy != 0) {
    int destx = owner->x + dx;
    int desty = owner->y + dy;
    if (engine.map->canWalk(destx, desty)) {
      owner->x = destx;
      owner->y = desty;
    } else {
      Entity* actor = engine.getEntity(destx, desty);
      if (actor) {
        owner->attacker->attack(owner, actor);
      }
    }
  }
  TemporaryAi::update(owner);
}
