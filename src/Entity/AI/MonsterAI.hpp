#pragma once

#include "Entity/Entity.hpp"

class MonsterAi : public Ai {
public :
    MonsterAi();
    void update(Entity* owner);
protected :
    int moveCount;

    void moveOrAttack(Entity* owner, int targetx, int targety);
};
