#include <stdio.h>

#include <libtcod.hpp>
#include "Entity/Entity.hpp"
#include "Entity/AI/PlayerAI.hpp"
#include "Map.hpp"
#include "Gui.hpp"
#include "Engine.hpp"

void PlayerAi::update(Entity* owner) {
  if ( owner->destructible && owner->destructible->isDead() ) {
    return;
  }
  int dx=0, dy=0;
  switch(engine.lastKey.vk) {
    case TCODK_UP : dy=-1; break;
    case TCODK_DOWN : dy=1; break;
    case TCODK_LEFT : dx=-1; break;
    case TCODK_RIGHT : dx=1; break;
    case TCODK_CHAR : handleActionKey(owner, engine.lastKey.c); break;
    default:break;
  }
  if (dx != 0 || dy != 0) {
    engine.gameStatus=Engine::NEW_TURN;
    if (moveOrAttack(owner, owner->x+dx,owner->y+dy)) {
      engine.map->computeFov();
    }
  }
}

bool PlayerAi::moveOrAttack(Entity* owner, int targetx,int targety) {
  if ( engine.map->isWall(targetx,targety) ) return false;
  // look for living actors to attack
  for (Entity** iterator = engine.entities.begin(); iterator != engine.entities.end(); iterator++) {
    Entity* actor = *iterator;
    if ( actor->destructible && !actor->destructible->isDead() && actor->x == targetx && actor->y == targety ) {
      owner->attacker->attack(owner, actor);
      return false;
    }
  }
  // look for corpses or items
  for (Entity** iterator = engine.entities.begin(); iterator != engine.entities.end(); iterator++) {
    Entity* actor = *iterator;
    bool corpseOrItem = (actor->destructible && actor->destructible->isDead()) || actor->pickable;
    if ( corpseOrItem && actor->x == targetx && actor->y == targety ) {
      engine.gui->message(TCODColor::lightGrey,"There's a %s here.", actor->name());
    }
  }
  owner->x=targetx;
  owner->y=targety;
  return true;
}

void PlayerAi::handleActionKey(Entity* owner, int ascii) {
  // TODO: make keybindings configurable
  if (ascii == 'd') {
    Entity* actor = choseFromInventory(owner);
    if ( actor ) {
      actor->pickable->drop(actor,owner);
      engine.gameStatus=Engine::NEW_TURN;
    }
  } else if (ascii == 'g') {
    bool found=false;
    for (Entity** iterator = engine.entities.begin(); iterator != engine.entities.end(); iterator++) {
      Entity* actor = *iterator;
      if (actor->pickable && actor->x == owner->x && actor->y == owner->y) {
        if (actor->pickable->pick(actor,owner)) {
          found=true;
          engine.gui->message(TCODColor::lightGrey,"%s pick up the %s.", owner->name(), actor->name());
          break;
        } else if (! found) {
          found=true;
          engine.gui->message(TCODColor::red,"Your inventory is full.");
        }
      }
    }
    if (!found) {
      engine.gui->message(TCODColor::lightGrey,"There's nothing here that you can pick.");
    }
    engine.gameStatus=Engine::NEW_TURN;
  } else if (ascii == 'i') {
    Entity* actor = this->choseFromInventory(owner);
    if (actor) {
      actor->pickable->use(actor,owner);
      engine.gameStatus=Engine::NEW_TURN;
    }
  } else if (ascii == 's') {
    // TODO: show skills dialog

  } else if (ascii == 't') {
    Entity* target = engine.targetPerson(10);
    if (target) {
      engine.gui->message(TCODColor::lightGrey, "You chat with the %s.", target->name());
      engine.gui->message(TCODColor::red, "TODO: implement chat dialog");
      // TODO: render talking gui
    }
  } else if (ascii == '?') {
    // TODO: show help dialog
  }
}

Entity* PlayerAi::choseFromInventory(Entity *owner) {
  int index = engine.gui->listSelector(owner->container->inventory);
  if (index < 0) {
    return nullptr;
  }
  return owner->container->inventory.get(index);
}
