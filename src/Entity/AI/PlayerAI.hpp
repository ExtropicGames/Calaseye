#pragma once

#include "Entity/Entity.hpp"

class PlayerAi : public Ai {
public :
    void update(Entity* owner);

protected :
    bool moveOrAttack(Entity* owner, int targetx, int targety);
    void handleActionKey(Entity* owner, int ascii);
    Entity* choseFromInventory(Entity* owner);
};
