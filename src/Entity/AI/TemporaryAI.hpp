#pragma once

class TemporaryAi : public Ai {
public :
    TemporaryAi(int nbTurns);
    void update(Entity* owner);
    void applyTo(Entity* actor);
protected :
    int nbTurns;
    Ai* oldAi;
};
