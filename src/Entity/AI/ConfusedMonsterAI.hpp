#pragma once

#include "Entity/Entity.hpp"

class ConfusedMonsterAi : public TemporaryAi {
public :
    ConfusedMonsterAi(int nbTurns);
    void update(Entity* owner);
};
