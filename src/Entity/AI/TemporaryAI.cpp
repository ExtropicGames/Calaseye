#include "Entity/Entity.hpp"
#include "Entity/AI/TemporaryAI.hpp"

TemporaryAi::TemporaryAi(int nbTurns) : nbTurns(nbTurns) {
}

void TemporaryAi::update(Entity* owner) {
  nbTurns--;
  if ( nbTurns == 0 ) {
    owner->ai = oldAi;
    delete this;
  }
}

void TemporaryAi::applyTo(Entity* actor) {
  oldAi=actor->ai;
  actor->ai=this;
}
