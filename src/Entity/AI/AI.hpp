#pragma once

class Entity;

class Ai {
public :
    virtual void update(Entity* owner)=0;
};