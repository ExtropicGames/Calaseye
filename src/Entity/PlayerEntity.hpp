#pragma once

class PlayerEntity : public Entity {
public:
  PlayerEntity(int x, int y, int ch, const char *name, const TCODColor &col) : Entity(x, y, ch, name, col) {
  }
  virtual const char* name() { return "you"; }
  virtual const char* properName() { return _name; }
};
