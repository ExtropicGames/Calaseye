#include <math.h>
#include <libtcod.hpp>
#include "Entity.hpp"

Entity::Entity(int x, int y, int ch, const char *name, const TCODColor &col) :
  x(x),y(y),ch(ch),col(col),_name(name),
  blocks(true),attacker(nullptr),destructible(nullptr),ai(nullptr),
  pickable(nullptr),container(nullptr) {
}

Entity::~Entity() {
  if ( attacker ) delete attacker;
  if ( destructible ) delete destructible;
  if ( ai ) delete ai;
  if ( pickable ) delete pickable;
  if ( container ) delete container;
}

void Entity::render() const {
  TCODConsole::root->setChar(x,y,ch);
  TCODConsole::root->setCharForeground(x,y,col);
}

void entity_update(Entity* e) {
  if (e->ai) {
    e->ai->update(e);
  }
}

bool is_alive(const Entity* e) {
  return e->destructible && !e->destructible->isDead();
}

float get_distance(const Entity* e, int x, int y) {
  int dx = e->x - x;
  int dy = e->y - y;
  return sqrtf(dx * dx + dy * dy);
}
