#pragma once

#include "Entity/Entity.hpp"
#include "Map.hpp"
#include "Gui.hpp"

class Engine {
public :
    enum GameStatus {
        STARTUP,
        IDLE,
        NEW_TURN,
        VICTORY,
        DEFEAT,
    } gameStatus;
    TCOD_key_t lastKey;
    TCOD_mouse_t mouse;
    TCODList<Entity*> entities;
    Entity* player;
    Map* map;
    int fovRadius;
    int screenWidth;
    int screenHeight;
    Gui* gui;

    Engine(int screenWidth, int screenHeight);
    ~Engine();
    void update();
    void render();
    void sendToBack(Entity* entity);
    Entity* getEntity(int x, int y) const;
    Entity* getClosestMonster(int x, int y, float range) const;
    bool pickATile(int *x, int *y, float maxRange = 0.0f);
    Entity* targetPerson(float maxRange = 0.0f);
};

extern Engine engine;
